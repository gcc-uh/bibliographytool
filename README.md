
<!-- README.md is generated from README.Rmd. Please edit that file -->

# bibliographyTool

<!-- badges: start -->
<!-- badges: end -->

Package `bibliographyTool`aims at facilitating the download and the
exploration of abstracts and titles from bibliography in Elsevier/Scopus
(using package `rscopus`).

## Installation

You can install the package from GitLab.

``` r
devtools::install_git("https://gitlab.com/gcc-uh/bibliographytool")
```

## Example

A short example using the `analyse_article` function.

``` r
library(bibliographyTool)

#these are examples of a title and abstract retrieved from Elsevier/Scopus:
article_title <- "Toward a holistic understanding of pastoralism"
article_abstract <- "© 2021 Elsevier Inc.Pastoralism is globally significant in social, environmental, and economic terms. However, it experiences crises rooted in misconceptions and poor interdisciplinary understanding, while being largely overlooked in international sustainability forums and agendas. Here, we propose a transdisciplinary research approach to understand pastoralist transitions using (1) social, economic, and environmental dimensions, (2) diverse geographic contexts and scales to capture emerging properties, allowing for cross-system comparisons, and (3) timescales from the distant past to the present. We provide specific guidelines to develop indicators for this approach, within a social-ecological resilience analytical framework to understand change. Distinct systems undergo similar transitions over time, crossing critical thresholds and then either collapsing or recovering. Such an integrated view of multidimensional interactions improves understanding of possible tipping points, thereby supporting better-informed decision making. The need for a paradigm shift in pastoralism science and policy is pressing. This research approach, including participatory methods, can provide the solutions urgently needed."

#if you want the function analyse_article() to compute a score of predefined words or expression, you must create a list of such words/expressions:
special_words <- c("transitions", "change", "policy")
special_expressions <- c("climate change")

analyse_article(article_title, article_abstract, special.words = special_words, special.expressions = special_expressions)
```

<img src="man/figures/README-example-1.png" width="100%" />

    #> $most.relevant.words
    #> 
    #>    approach      social pastoralism    research 
    #>           3           3           2           2 
    #> 
    #> $score
    #> [1] 3

## Downloading abstracts from Scopus

The following code shares an example on how to download info from
Elsevier/Scopus (e.g. article title, abstracts, keywords etc.).

``` r
#Important 01: the local network needs to have access to Elsevier/Scopus search engine. So you need to be in a university facility with access to Scopus (or being using a VPN connection) to perform these kind of searches in R (just like in a web browser).

#Important 02: Before running the following code lines, obtain an API Key following this simple instructions: https://cran.r-project.org/web/packages/rscopus/vignettes/api_key.html

library(rscopus)

rscopus::set_api_key("PASTE YOUR API KEY HERE")
print(get_api_key(), reveal=TRUE) #confirm it is now in R
have_api_key() #should return TRUE

### An example of Scopus query (modify as needed)
### Reading https://dev.elsevier.com/sc_search_tips.html is important!

result_scopus <- scopus_search(query = "title-abs-key(pastoralism  AND  transdisciplinar*  AND  change)") 
length(result_scopus[[1]]) #number of entries

#load these two temporary functions (they will be handy in the following analyses):

keywords_function <- function (x) { #temporary function, needs more validation!
    tmp <- x$content$`abstracts-retrieval-response`$authkeywords$'author-keyword'
    res <- NULL
    for (i in 1:length(tmp)) {
        res <- c(res, tmp[[i]]$'$')
    }
    return(paste(res, collapse = ", "))
}

authors_function <- function (x) { #temporary function, needs more validation!
  tmp <- x$content$`abstracts-retrieval-response`$authors$author
  res <- NULL
  for (i in 1:length(tmp)) {
    res <- c(res, tmp[[i]]$'ce:indexed-name')
  }
  return(paste(res, collapse = ", "))
}

#if you want the function analyse_article() to compute a score of predefined words or expression, you must create a list of such words/expressions:

special_words <- c("transitions", "change", "policy")
special_expressions <- c("climate change")

#run this cycle, so you can inspect all the title and abstracts you downloaded from Scopus, keeping record of those you want to further analyse.

#create an empty data frame
Final_data_frame <- data.frame(number = integer(), title = character(), abstract = character(), keywords = character(), authors = character(), 'further_analysis_or_comments' = character())

for (i in 1:length(result_scopus[[1]])) {
  #i <- 3 #changing the value of i any abstract can be retrieved, using the function abstract_retrieval(), and inspected directly in R.
  abstract_i <-  abstract_retrieval(result_scopus[[1]][[i]]$`dc:identifier`, identifier = "scopus_id", verbose = FALSE)

  #abstract_i$content$`abstracts-retrieval-response`$item$bibrecord$head$grantlist$`grant-text`$`$`
  #abstract_i$content$`abstracts-retrieval-response`$item$bibrecord$head$`citation-title`
  #abstract_i$content$`abstracts-retrieval-response`$item$bibrecord$head$abstracts

  article_title <- abstract_i$content$`abstracts-retrieval-response`$coredata$`dc:title`
  article_abstract <- abstract_i$content$`abstracts-retrieval-response`$coredata$`dc:description`
  article_keywords <- keywords_function(abstract_i) #I notice that several articles don't have author keywords but have indexed keywords (I still couldn't find where are indexed keywords...)
  article_authors <- authors_function(abstract_i)

  #allows you to visualize the title, abstract, keywords and authors in the console
  cat("TITLE:", article_title, "\n\n")
  cat("ABSTRACT:", article_abstract, "\n\n")
  cat("KEYWORDS:", article_keywords, "\n\n")
  cat("AUTHORS:", article_authors)

  #function analyse_article() can now be used to get a picture of it and get the score using the predefined words
  par(mar=c(0,0,0,0))
  analyse_article(article_title, article_abstract, full.output=TRUE, special.words = " ", special.expressions = " ")

  answer <- readline("Further analyse this article? (y/n/text with comment/q, to break):")
  if (answer == "y") {Final_data_frame <- rbind(Final_data_frame, data.frame(number= i, title = article_title, abstract = article_abstract, keywords = article_keywords, authors = article_authors, 'further analysis/comments' = "Yes"))}
  if (answer == "n") {Final_data_frame <- rbind(Final_data_frame, data.frame(number= i, title = article_title, abstract = article_abstract, keywords = article_keywords, authors = article_authors, 'further analysis/comments' = "No"))}
  if (!answer %in% c("q", "y", "n")) {Final_data_frame <- rbind(Final_data_frame, data.frame(number= i, title = article_title, abstract = article_abstract, keywords = article_keywords, authors = article_authors, 'further analysis/comments' = answer))}
  if (answer == "q") {break} #if you break the cycle you will end up with an incomplete data frame. Yet, you can resume after modifying the for cycle input.
}

#Finally, check the result using e.g. tibble for neater printing:
tibble::tibble(Final_data_frame)

#Don't forget to save your work

### Some other examples of queries in Elsevier/Scopus (modify as needed):

#result_scopus <- scopus_search(query = "all(mammal AND carnivore AND tracking)")
#result_scopus_title <- scopus_search(query = "title(mammal AND carnivore AND tracking)")
#result_scopus_abs <- scopus_search(query = "abs(mammal AND carnivore AND tracking)")
#result_scopus_key <- scopus_search(query = "key(mammal AND carnivore AND tracking)")
#result_scopus_TAK <- scopus_search(query = "title-abs-key(mammal AND carnivore AND tracking)")
```
