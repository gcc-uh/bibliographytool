% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/analyse_article.R
\name{analyse_article}
\alias{analyse_article}
\title{Text analysis of an article title and abstract}
\usage{
analyse_article(
  title,
  abstract,
  plot = TRUE,
  POS = c("Noun"),
  full.output = FALSE,
  quantile.prob = 0.75,
  calculate.score = TRUE,
  special.words,
  special.expressions
)
}
\arguments{
\item{title}{\code{character} The title of the article.}

\item{abstract}{\code{character} The abstract of the article.}

\item{plot}{\code{logical} If \code{TRUE} (default) the result of the text analysis is presented in a plot using \code{\link[wordcloud]{wordcloud}} function; \code{FALSE} disables plotting.}

\item{POS}{\code{character} The 'part of speech' (grammatical category) of the words to search for (defaults no "Noun", which seems to work appropriately).}

\item{full.output}{\code{logical} If \code{TRUE} all found nouns are returned; if \code{FALSE} (default) only the most frequent nouns are returned (see quantile.prob argument).}

\item{quantile.prob}{\code{numeric} The probability value in \verb{[0, 1]} to be passed to function \code{\link[stats]{quantile}}, to produce the corresponding sample quantile, which is used to get the most frequent nouns (defaults to 0.75).}

\item{calculate.score}{\code{logical} If \code{TRUE} (default) the score of special words and expressions is calculated; if \code{FALSE} the score is not calculated.}

\item{special.words}{\verb{character vector} A vector of particular words that will be searched in the title+abstract and used to calculate a score (see Details).}

\item{special.expressions}{\verb{character vector} A vector of particular expressions that will be searched in the title+abstract and used to calculate a score (see Details).}
}
\value{
For full.output = \code{FALSE}, a \code{list} with the frequency of the most relevant words (obtained with the \code{quantile.prob}), and the score of special words and expressions (if calculate.score = \code{TRUE}).
For full.output = \code{TRUE}, a \code{list} with the frequency of all words, and the score of special words and expressions (if calculate.score = \code{TRUE}).
}
\description{
This function accepts the title and abstract of an article and performs a text analysis on them. The result of the analysis can be presented as an wordcloud (using \code{\link[wordcloud]{wordcloud}} function).
}
\details{
The \code{quantile.prob} argument is only used for plot = TRUE and for full.output = \code{FALSE}.
Words appearing only once in the title + abstract (i.e. frequency of 1) are always removed from the wordcloud plots. Such words are also not returned when full.output = \code{FALSE}. To inspect the words occurring only once, use full.output = \code{TRUE}.

This function uses a modified version of the \code{\link[tidytext]{parts_of_speech}} database from the \code{tidytext} package.

A score is calculated counting the number of "special words" and the number of "special expressions" that are found at least once in the title + abstract.
}
\author{
Tiago Monteiro-Henriques. E-mail: \email{tiagomonteirohenriques@gmail.com}. CITAB-UTAD. GCC-UH.
}
